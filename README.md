## Projeto feito como parte do desafio mobile do processo de trainee CompJúnior
<!-- blank -->
### Setup
`npm install` <br>
`expo start`
### Variáveis de desenvolvimento
API_URL=url base da api, exemplo: https://localhost:3000

#### OBS
A integração Backend do código foi feita com a API do desafio backend (https://gitlab.com/matheusoliveira3631/deafio_backend)
