import axios from './Axios'

export const Login = async (email, password)=>{
    try{
        await axios.post('/login', {
            email,
            password
        })
        return true
    }catch(err){
        console.error
        return false
    }
}

