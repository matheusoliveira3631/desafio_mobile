import axios from './Axios'

const RegisterUser = async (data, success, failed)=>{
    const res = await axios.post('/users', {
        username:data.username,
        email:data.email,
        password:data.password,
        admin:'false'
    }).catch((err)=>{
        console.log(err.toJSON())
    })
    success()
    
}

export default RegisterUser