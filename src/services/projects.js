//teoricamente os dados seriam obtidos através de uma api, mas quem não tem cão, caça com gato
//troquei os nomes das empresas por motivos legais

export const projectData = [
    {
      "date": "14/05 - 02/09/2021",
      "name": "Masig"
    },
    {
      "date": "10/05 - 30/07/2021",
      "name": "SSH"
    },
    {
      "date": "26/03 - 15/06/2021",
      "name": "Projeto Nayara"
    },
    {
      "date": "18/02 - 28/04/2021",
      "name": "ConstruRios"
    },
    {
      "date": "08/02 - 04/06/2021",
      "name": "PS 2021.1"
    },
    {
      "date": "29/01 - 09/07/2021",
      "name": "AyurMar"
    },
    {
      "date": "01/10 - 31/12/2020",
      "name": "Site do Processo Seletivo [ Interno ]"
    },
    {
      "date": "01/10 - 31/12/2020",
      "name": "Podcast da Comp [ Interno ]"
    },
    {
      "date": "01/09 - 31/12/2020",
      "name": "Daniela Marçal & (Des)Associados"
    },
    {
      "date": "03/08 - 17/12/2020",
      "name": "S6IS [MKT]"
    }
  ]