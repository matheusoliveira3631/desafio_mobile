import  {StyleSheet} from 'react-native'

export const styles = StyleSheet.create({
    container:{
        backgroundColor:'white',
        borderRadius:5,
        alignSelf:'center',
        margin:15,
        flex:1,
        width:'100%',
        maxHeight:365
        
    },
    text:{
        color:'#2196f3',
        textAlign:'center',
        fontSize:18
    }
})