import React from 'react'
import {View, Text, FlatList} from 'react-native'

import {Project } from '../project/project'
import { projectData } from '../../services/projects'

import {styles} from './styles'

export const Projects = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.text}>Projetos</Text>
            <FlatList
                data={projectData}
                renderItem={ ({ item, index }) => (
                    <Project props={{title:item.name, subtitle:item.date}}/>
                  )}
            />
        </View>
    )
    }