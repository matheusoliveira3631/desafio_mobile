import React, {useRef, useState, useEffect} from 'react';
import {View, FlatList, Image, Text} from 'react-native'

import {Card} from '../card/card'

import {styles} from './styles'

export const CardCarousel = props => {
  
  const data=[
    require('../../assets/images/ps.png'),
    require('../../assets/images/comp_week.png'),
    require('../../assets/images/logo_full.png')  
    
  ]


  return (
    <FlatList
    horizontal={true} 
    showsHorizontalScrollIndicator={false} 
    data={data}
    renderItem={ ({ item, index }) => (
      <Card source={item} key={index}/>
    )}
  />
  );
};


