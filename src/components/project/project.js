import React from "react";
import {Text, View} from 'react-native'

export const Project = ({props})=>{
    return (
    <View style={{padding:5, borderBottomColor:'#7d7d7d', borderBottomWidth:'.5px'}}>
    <Text style={{fontSize:16, marginBottom:5}}>{props.title}</Text>
    <Text style={{color:'#7d7d7d', fontSize:14}}>Deadline: {props.subtitle}</Text>
    </View>
    )
}