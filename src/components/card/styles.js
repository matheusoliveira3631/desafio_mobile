import {StyleSheet} from 'react-native'

export const styles=StyleSheet.create({
    container:{
      margin:5,
      borderRadius:16,
    },
      image: {
        height:'180px',
        width:'95vw',
        borderRadius:16,
        resizeMode:'cover'
      }
})