import React from "react"
import {Image, Text, View} from 'react-native'
import {styles} from './styles'

export const Card = ({source}) => {
    return (
      <View style={styles.container}>
        <Image source={source} style={styles.image}/>
        </View>
    )
}