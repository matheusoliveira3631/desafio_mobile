import {StyleSheet} from 'react-native'

export const styles=StyleSheet.create({
    container:{
        width:'100%',
        height:'7.5vh',
        backgroundColor:'#218fa6',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        padding:'25px'
    },
    image:{
        width:'25vw',
        aspectRatio: 4/1,
        resizeMode:'stretch',
    },
    profilePic:{
        width:'10vw',
        aspectRatio: 1/1,
        resizeMode:'stretch',
        borderRadius:'50%',
    }

})