import React from 'react'
import { View, Image, TouchableWithoutFeedback} from 'react-native'
import { styles } from './styles'

export const Header  =({source})=>{

    return(
        <View style={styles.container}>
                <Image style={styles.image} source={require(`../../assets/images/logo_white.png`)}/>
                <Image style={styles.profilePic} source={source}/>  
            </View>
    )
}