import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
//components
import {Login} from '../views/LoginScreen/LoginScreen'
import {Register} from '../views/RegisterScreen/RegisterScreen'
import {Reminder} from '../views/PasswordReminder/PasswordReminder'
//

const Stack = createNativeStackNavigator();

export const Common =  ()=>{
    return (
        <Stack.Navigator initialRouteName='Login'>
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name='Register' component={Register}/>
          <Stack.Screen name='Reminder' component={Reminder}/>
        </Stack.Navigator>     
    )
}