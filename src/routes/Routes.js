import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {Common} from './common'
import { Authenticated } from './authenticated';
import { AuthProvider, authContext } from '../contexts/authContext';

const SwitchContext = ()=>{
  const {isUser} = authContext()
  return isUser ? <Authenticated/> : <Common/>
}

export default ()=>{
    return (
        <NavigationContainer> 
          <AuthProvider>
            <SwitchContext/>
          </AuthProvider>
        </NavigationContainer> 
    )
}


