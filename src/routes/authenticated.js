import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
//
import {Main} from '../views/mainScreen/mainScreen'

const Stack = createNativeStackNavigator();

export const Authenticated =  ()=>{
    return (
        <Stack.Navigator initialRouteName='Main'>
          <Stack.Screen name='Main' component={Main}/>
        </Stack.Navigator>     
    )
}