import React, {createContext, useContext, useState} from 'react'
import {Login} from '../services/auth'

const Context=createContext({})

export const authContext = ()=> useContext(Context)

export const AuthProvider = ({children})=>{
    const [isUser, setUser] = useState(false)
    const [loginError, setLoginError] = useState(false)

    const login = (email, pwd)=>{
        Login(email, pwd).then(res=>{
            res ? setUser(true) : setLoginError(true)
        }).catch(err=>{
            console.error   
        })
    }

    const logout = ()=>{
        setUser(false)
    }

    return <Context.Provider value={{isUser, loginError, login, logout}}>{children}</Context.Provider>
}