import React, { Component } from 'react'
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  Image,
  StatusBar,
  Button,
  TextInput
} from 'react-native'
import { useForm, Controller } from "react-hook-form"
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from "yup"
import { authContext } from '../../contexts/authContext'


import {styles} from './styles'

export const Login = ({navigation})=>{
  const schema = yup.object({
    email: yup.string().email('Informe um email válido').required('Email é obrigatório'),
    password: yup.string().required('Senha é obrigatória')
  }).required();

  const {control, handleSubmit, formState: { errors } } = useForm({
    resolver:yupResolver(schema)
  });

  const {login, loginError} = authContext()

  const onSubmit = data =>{login(data.email, data.password)}
 
    return (
      <View style={styles.container}> 
      <StatusBar style="auto"></StatusBar>
        <View style={styles.imgContainer}>
        <Image style={styles.image} source={require(`../../assets/images/logo.png`)}/>
        </View>
        {loginError && <Text style={styles.loginError}>Email ou senha incorretos</Text>}
        <View style={styles.loginContainer}> 
        <Text style={styles.loginText}>Login</Text>
      <Controller
        control={control}
        rules={{
         required: true,
        }}
        render={({ field: { onChange, onBlur, value } }) => (
          <TextInput
            style={styles.input}
            onBlur={onBlur}
            onChangeText={onChange}
            value={value}
            placeholder='Email'
          />
        )}
        name="email"
        defaultValue=""
      />
      {errors.email && <Text style={styles.error}>{errors.email.message}</Text>}

      <Controller
        control={control}
        render={({ field: { onChange, onBlur, value } }) => (
          <TextInput
            style={styles.input}
            onBlur={onBlur}
            onChangeText={onChange}
            value={value}
            secureTextEntry
            placeholder='Senha'
          
          />
        )}
        name="password"
        defaultValue=""
      />
      {errors.password && <Text style={styles.error}>{errors.password.message}</Text>}
      <View style={styles.Inputbutton}>
      <Button title="Login"  onPress={handleSubmit(onSubmit)} />
      </View>
      <View >
        <Text style={styles.register} onPress={()=>{
            navigation.push('Register')
        }}>
          Criar conta
        </Text>
      </View>
      <View >
        <Text style={styles.register} onPress={()=>{
            navigation.push('Reminder')
        }}>
          Esqueci minha senha
        </Text>
      </View>
    </View>
      </View>
    )
  }                                                    




