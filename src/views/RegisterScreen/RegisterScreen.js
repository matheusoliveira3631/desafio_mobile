import React, { Component } from 'react'
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  Image,
  StatusBar,
  Button,
  TextInput
} from 'react-native'
import { useForm, Controller } from "react-hook-form"
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from "yup"
import RegisterUser from '../../services/register'


import {styles} from './styles'

export const Register = ({navigation})=>{
  const schema = yup.object({
    username: yup.string().required('Nome de usuário é obrigatório'),
    email: yup.string().email('Informe um email válido').required('Email é obrigatório'),
    password: yup.string().required('Senha é obrigatória')
  }).required()

  const {control, handleSubmit, formState: { errors } } = useForm({
    resolver:yupResolver(schema)
  })

  const onSubmit = data =>{RegisterUser(data, ()=>{
    navigation.navigate('Login')
  })}


 
    return (
      <View style={styles.container}> 
      <StatusBar style="auto"></StatusBar>
        <View style={styles.imgContainer}>
        <Image style={styles.image} source={require(`../../assets/images/logo.png`)}/>
        </View>
        <View style={styles.loginContainer}> 
        <Text style={styles.loginText}>Criar conta</Text>
        <Controller
        control={control}
        rules={{
         required: true,
        }}
        render={({ field: { onChange, onBlur, value } }) => (
          <TextInput
            style={styles.input}
            onBlur={onBlur}
            onChangeText={onChange}
            value={value}
            placeholder='Nome de usuário'
          />
        )}
        name="username"
        defaultValue=""
      />

      {errors.username && <Text style={styles.error}>{errors.username.message}</Text>}

      <Controller
        control={control}
        rules={{
         required: true,
        }}
        render={({ field: { onChange, onBlur, value } }) => (
          <TextInput
            style={styles.input}
            onBlur={onBlur}
            onChangeText={onChange}
            value={value}
            placeholder='Email'
          />
        )}
        name="email"
        defaultValue=""
      />
      {errors.email && <Text style={styles.error}>{errors.email.message}</Text>}

      <Controller
        control={control}
        render={({ field: { onChange, onBlur, value } }) => (
          <TextInput
            style={styles.input}
            onBlur={onBlur}
            onChangeText={onChange}
            value={value}
            placeholder='Senha'
            secureTextEntry
          />
        )}
        name="password"
        defaultValue=""
      />
      {errors.password && <Text style={styles.error}>{errors.password.message}</Text>}
      <View style={styles.Inputbutton}>
      <Button title="Criar conta" onPress={handleSubmit(onSubmit)} />
      </View>
      </View>
    </View>
      
    )
  }                                                    




