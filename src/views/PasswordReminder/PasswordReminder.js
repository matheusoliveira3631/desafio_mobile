import React, { Component, useState } from 'react'
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  Image,
  StatusBar,
  Button,
  TextInput
} from 'react-native'
import { useForm, Controller } from "react-hook-form"
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from "yup"
import axios from '../../services/Axios'


import {styles} from './styles'

export const Reminder = ({navigation})=>{
  const schema = yup.object({
    email: yup.string().email('Informe um email válido').required('Email é obrigatório')
  }).required();

  const {control, handleSubmit, formState: { errors } } = useForm({
    resolver:yupResolver(schema)
  });

  const [emailError, setEmailError] = useState(false)
  const [emailSuccess, setEmailSuccess] = useState(false)

  const onSubmit = async (data) =>{
    try{
        await axios.get('/users/reminder',{params:{email:data.email}})
        setEmailSuccess(true)
        setTimeout(() => {
          navigation.navigate('Login')
        }, 1000);
    }catch(err){
      setEmailError(true)
      setTimeout(() => {
        setEmailError(false)
      }, 2000);
    }
  }

 
    return (
      <View style={styles.container}> 
      <StatusBar style="auto"></StatusBar>
        <View style={styles.imgContainer}>
        <Image style={styles.image} source={require(`../../assets/images/logo.png`)}/>
        </View>
        <View style={styles.loginContainer}> 
        <Text style={styles.loginText}>Recuperação de senha</Text>
      <Controller
        control={control}
        rules={{
         required: true,
        }}
        render={({ field: { onChange, onBlur, value } }) => (
          <TextInput
            style={styles.input}
            onBlur={onBlur}
            onChangeText={onChange}
            value={value}
            placeholder='Email'
          />
        )}
        name="email"
        defaultValue=""
      />
      {errors.email && <Text style={styles.error}>{errors.email.message}</Text>}
      {emailError && <Text style={styles.error}>Não há usuário com este email</Text>}
      {emailSuccess && <Text style={styles.register}>Email de recuperação enviado</Text>}
      <View style={styles.Inputbutton}>
      <Button title="Recuperar senha"  onPress={handleSubmit(onSubmit)} />
      </View>
      
    </View>
      </View>
    )
  }                                                    




