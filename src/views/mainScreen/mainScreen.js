import React, { Component } from 'react'
import {View} from 'react-native'
//
import {Header} from '../../components/header/header'
import {CardCarousel} from '../../components/carousel/carousel'
import { Projects } from '../../components/projects/projects'

import { styles } from './styles'

export const Main = ({navigation})=>{
    return(
        <View style={styles.container}>
        <Header source={require('../../assets/images/user.png')}></Header>
        <View>
        <CardCarousel />  
        </View>
        <View style={{padding:25}}>
        <Projects />
        </View>
        </View>
    )
}